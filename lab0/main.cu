#include <cstdio>
#include <cstdlib>
#include "SyncedMemory.h"

#define CHECK {\
	auto e = cudaDeviceSynchronize();\
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e));\
		abort();\
	}\
}

const int W = 40;
const int H = 12;

__global__ void Draw(char *frame) {
	const int y = blockIdx.y * blockDim.y + threadIdx.y;
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
   const char *draw = ":::::::::::::::::::::::::::::::::::::::\n\
:                                     :\n\
:                                     :\n\
:                                     :\n\
:                                     :\n\
:                               <|    :\n\
:                                |    :\n\
:                                |    :\n\
:                                |    :\n\
:                                |    :\n\
:                                #    :\n\
:::::::::::::::::::::::::::::::::::::::";
   if (y < H and x < W) {
		frame[y*W+x] = draw[y*W+x];
	}
   if (y > 4 && y < 11) {
      if (x < 22 && x >= 22 - (2 * y - 6))
         frame[y*W+x] = '#';
   }
}

int main(int argc, char **argv)
{
	MemoryBuffer<char> frame(W*H);
	auto frame_smem = frame.CreateSync(W*H);
	CHECK;

	Draw<<<dim3((W-1)/16+1,(H-1)/12+1), dim3(16,12)>>>(frame_smem.get_gpu_wo());
	CHECK;

	puts(frame_smem.get_cpu_ro());
	CHECK;
	return 0;
}
