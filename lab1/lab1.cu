#include "lab1.h"
static const unsigned W = 640;
static const unsigned H = 480;
static const unsigned NFRAME = 1104;
static const unsigned CSIZE = 4;
static const int CW = W / CSIZE;
static const int CH = H / CSIZE;

struct Lab1VideoGenerator::Impl {
	int t = 0;
	uint8_t *alive, *output;
};

Lab1VideoGenerator::Lab1VideoGenerator(): impl(new Impl) {
	cudaMalloc((void **) &impl->alive, sizeof(uint8_t)*CW*CH);
	cudaMalloc((void **) &impl->output, sizeof(uint8_t)*CW*CH);
	uint8_t *tmp = (uint8_t*)malloc(sizeof(uint8_t)*CW*CH);
	memset(tmp, 0, sizeof(uint8_t)*CW*CH);
	int y, x;
	FILE *f = fopen("idx.txt", "r");
	while (~fscanf(f, "%d %d", &y, &x))
	{
		tmp[(y+90)*CW+x+130] = 1;
		tmp[(30-y)*CW+x+130] = 1;
		tmp[(30-y)*CW+30-x] = 1;
		tmp[(y+90)*CW+30-x] = 1;
	}
	fclose(f);
	cudaMemcpy(impl->alive, tmp, sizeof(uint8_t)*CW*CH, cudaMemcpyHostToDevice);
	free(tmp);
}

Lab1VideoGenerator::~Lab1VideoGenerator() {
	cudaFree(impl->alive);
	cudaFree(impl->output);
}

void Lab1VideoGenerator::get_info(Lab1VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
	info.fps_n = 24;
	info.fps_d = 1;
};

__global__ void draw(uint8_t *frame, uint8_t *alive, uint8_t *output) {
	const int y = blockIdx.y * blockDim.y + threadIdx.y;
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
	const int cx = x / CSIZE;
	const int cy = y / CSIZE;
	const int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
	const int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

	if (y < H && x < W) {
		int count = 0;
		for (int i = 0; i < 8; i++) {
			int nx = cx + dx[i];
			int ny = cy + dy[i];
	
			if (nx >= CW) nx = 0;
			else if (nx < 0) nx = CW-1;
			if (ny >= CH) ny = 0;
			else if (ny < 0) ny = CH-1;
	
			if (alive[ny*CW+nx] == 1)
				count += 1;
		}
		if (count == 3 || count == 3 || (count == 2 && alive[cy*CW+cx] == 1))
			output[cy*CW+cx] = 1;
		else
			output[cy*CW+cx] = 0;
		
		frame[y*W+x] = output[cy*CW+cx] * 255;
		const int val = (alive[cy*CW+cx] - output[cy*CW+cx] + 1) * 127;
		for (int i = 0; i < 2; i++)
			for (int j = 0; j < 2; j++)
				frame[H*W+cy*W+cx*2+i*W/2+j] = val;
	}
}

void Lab1VideoGenerator::Generate(uint8_t *yuv) {
	cudaMemset(yuv, 0, W*H);
	cudaMemset(yuv+W*H, 128, W*H/2);
	draw<<<dim3((W-1)/16+1,(H-1)/12+1), dim3(16,12)>>>(yuv, impl->alive, impl->output);
	std::swap(impl->alive, impl->output);
	++(impl->t);
}
