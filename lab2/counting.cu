#include "counting.h"
#include <cstdio>
#include <cassert>
#include <algorithm>
#include <thrust/scan.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>
#define BLOCKSZ 1024

#define CHECK {\
	auto e = cudaDeviceSynchronize();\
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e));\
		abort();\
	}\
}

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

struct IsAlpha : public thrust::unary_function<char, int>
{
   __host__ __device__
   int operator()(char c) { return (c == '\n') ? 0 : 1; }
};

void CountPosition1(const char *text, int *pos, int text_size)
{
   thrust::device_ptr<const char> dev_text(text);
   thrust::device_ptr<int> dev_pos(pos);

   thrust::transform(dev_text, dev_text+text_size, dev_pos, IsAlpha());
   thrust::inclusive_scan_by_key(dev_pos, dev_pos+text_size, dev_pos, dev_pos);
}

__global__ void doubling(const char *text, int *pos, int text_size, int *sum)
{
    __shared__ int temp[BLOCKSZ*2];
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int thid = threadIdx.x;
    int tmpidx = 0;

    if (idx < text_size)
        temp[tmpidx*BLOCKSZ+thid] = (text[idx] == '\n') ? 0 : 1;
    __syncthreads();

    for (int offset = 1; offset <= 500; offset <<= 1)
    {
        if (idx < text_size && thid >= offset && temp[tmpidx*BLOCKSZ+thid] >= offset)
            temp[(1-tmpidx)*BLOCKSZ+thid] = temp[tmpidx*BLOCKSZ+thid] + temp[tmpidx*BLOCKSZ+thid-offset];
        else
            temp[(1-tmpidx)*BLOCKSZ+thid] = temp[tmpidx*BLOCKSZ+thid];
        __syncthreads();
        tmpidx = 1 - tmpidx;
    }

    if (idx < text_size)
        pos[idx] = temp[tmpidx*BLOCKSZ+thid];

    if (thid == blockDim.x - 1)
        sum[blockIdx.x] = temp[tmpidx*BLOCKSZ+thid];
}

__global__ void incr(int *pos, int text_size, int *sum)
{
    __shared__ int val;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int thid = threadIdx.x;

    if (thid == 0)
        val = (blockIdx.x > 0) ? sum[blockIdx.x-1] : 0;
    __syncthreads();

    if (idx < text_size)
    {
        if (pos[idx] == thid + 1)
            pos[idx] += val;
    }
}

void CountPosition2(const char *text, int *pos, int text_size)
{
   int nBlocks = CeilDiv(text_size, BLOCKSZ);
   int *sum;
   cudaMalloc(&sum, sizeof(int)*nBlocks);
   doubling<<<nBlocks, BLOCKSZ>>>(text, pos, text_size, sum);
   CHECK;
   incr<<<nBlocks, BLOCKSZ>>>(pos, text_size, sum);
   CHECK;
}
