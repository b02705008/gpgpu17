#include "lab3.h"
#include <cstdio>

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

__global__ void SimpleClone(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	if (yt < ht and xt < wt and mask[curt] > 127.0f) {
		const int yb = oy+yt, xb = ox+xt;
		const int curb = wb*yb+xb;
		if (0 <= yb and yb < hb and 0 <= xb and xb < wb) {
			output[curb*3+0] = target[curt*3+0];
			output[curb*3+1] = target[curt*3+1];
			output[curb*3+2] = target[curt*3+2];
		}
	}
}

__global__ void CalculateFixed(
	const float *background,
	const float *target,
	const float *mask,
	float *fixed,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	const int yb = oy+yt, xb = ox+xt;
	if (yb >= 0 && xb >= 0 && yt < ht && xt < wt && mask[curt] > 127.0f) {
		float val[3] = {0};
		if (0 <= yb && yb < hb && 0 <= xb && xb < wb) {
#define calculate(cond_b, cond_t, neit, neib) \
			if (cond_b) { \
				if (cond_t) { \
					val[0] += target[(curt)*3+0] - target[(neit)*3+0]; \
					val[1] += target[(curt)*3+1] - target[(neit)*3+1]; \
					val[2] += target[(curt)*3+2] - target[(neit)*3+2]; \
				} \
				if (!(cond_t) || mask[(neit)] <= 127.0f) { \
					val[0] += background[(neib)*3+0]; \
					val[1] += background[(neib)*3+1]; \
					val[2] += background[(neib)*3+2]; \
				} \
			}
			calculate(yb >= 1, yt >= 1, wt*(yt-1)+xt, wb*(yb-1)+xb);
			calculate(yb < hb - 1, yt < ht - 1, wt*(yt+1)+xt, wb*(yb+1)+xb);
			calculate(xb >= 1, xt >= 1, wt*yt+xt-1, wb*yb+xb-1);
			calculate(xb < wb - 1, xt < wt - 1, wt*yt+xt+1, wb*yb+xb+1);
		}
		fixed[curt*3+0] = val[0];
		fixed[curt*3+1] = val[1];
		fixed[curt*3+2] = val[2];
	}
}

__global__ void PoissonImageCloningIteration(
	const float *fixed,
	const float *mask, 
	const float *target,
	float *output,
	const int wt, const int ht, const int wb, const int hb,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	const int yb = oy+yt, xb = ox+xt;
	if (yb >= 0 && xb >= 0 && yt < ht && xt < wt && mask[curt] > 127.0f) {
		float val[3];
		int count = 0;

		val[0] = fixed[curt*3+0];
		val[1] = fixed[curt*3+1];
		val[2] = fixed[curt*3+2];

#define calculate2(cond_b, cond_t, neit) \
		if (cond_b) { \
			if ((cond_t) && mask[(neit)] > 127.0f) { \
				val[0] += target[(neit)*3+0]; \
				val[1] += target[(neit)*3+1]; \
				val[2] += target[(neit)*3+2]; \
			} \
			count += 1; \
		}

		calculate2(yb >= 1, yt >= 1, wt*(yt-1)+xt);
		calculate2(yb < hb - 1, yt < ht - 1, wt*(yt+1)+xt);
		calculate2(xb >= 1, xt >= 1, wt*yt+xt-1);
		calculate2(xb < wb - 1, xt < wt - 1, wt*yt+xt+1);

		output[curt*3+0] = val[0] / count;
		output[curt*3+1] = val[1] / count;
		output[curt*3+2] = val[2] / count;
	}
}

void PoissonImageCloning(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	float *fixed, *buf1, *buf2;
	cudaMalloc(&fixed, 3*wt*ht*sizeof(float));
	cudaMalloc(&buf1, 3*wt*ht*sizeof(float));
	cudaMalloc(&buf2, 3*wt*ht*sizeof(float));

	dim3 gdim(CeilDiv(wt,32), CeilDiv(ht,16)), bdim(32,16);
	CalculateFixed<<<gdim, bdim>>>(
	   background, target, mask, fixed,
	   wb, hb, wt, ht, oy, ox
	);
	cudaDeviceSynchronize();
	cudaMemcpy(buf1, target, sizeof(float)*3*wt*ht, cudaMemcpyDeviceToDevice);

	for (int i = 0; i < 10000; i++) {
		PoissonImageCloningIteration<<<gdim, bdim>>>(
		      fixed, mask, buf1, buf2, wt, ht, wb, hb, oy, ox
		);
		cudaDeviceSynchronize();
		PoissonImageCloningIteration<<<gdim, bdim>>>(
		      fixed, mask, buf2, buf1, wt, ht, wb, hb, oy, ox
		);
		cudaDeviceSynchronize();
	}
	cudaMemcpy(output, background, wb*hb*sizeof(float)*3, cudaMemcpyDeviceToDevice);
	SimpleClone<<<gdim, bdim>>>(
		background, buf1, mask, output,
		wb, hb, wt, ht, oy, ox
	);
	cudaDeviceSynchronize();

	cudaFree(fixed);
	cudaFree(buf1);
	cudaFree(buf2);
}
